package ca.ulex.puzzle;

import android.content.Context;
import android.graphics.Point;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    private Context context;
    private ImageMatrix imageMatrix;

    public ImageAdapter(Context c, ImageMatrix imageMatrix) {
        this.context = c;
        this.imageMatrix = imageMatrix;
    }

    public int getCount() {
        return imageMatrix.numberOfCells();
    }

    public Object getItem(int position) {
        return imageMatrix.getCell(position);
    }

    public long getItemId(int position) {
        return imageMatrix.getImageID();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(context);
            Point cellSize = imageMatrix.cellSize();
            imageView.setLayoutParams(new GridView.LayoutParams(cellSize.x, cellSize.y));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(1, 1, 1, 1);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageBitmap(imageMatrix.getCell(position));
        return imageView;
    }

}