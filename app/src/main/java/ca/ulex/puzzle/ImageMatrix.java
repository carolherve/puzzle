package ca.ulex.puzzle;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;

import java.util.Random;

public class ImageMatrix {
    private int imageID;
    private Point gridSize;
    private Point cellSize;
    private Point emptyCell;
    private Bitmap[][] bitmapMatrix;

    public ImageMatrix(int imageID, Point gridSize, Bitmap originalBitmap, Point displaySize)
    {
        this.imageID = imageID;
        this.gridSize = gridSize;
        this.cellSize = new Point();
        emptyCell = new Point(0, this.gridSize.y - 1);
        bitmapMatrix = new Bitmap[this.gridSize.y][this.gridSize.x];
        initializeImage(originalBitmap, displaySize);
    }

    public int getImageID()
    {
        return this.imageID;
    }

    public int numberOfCells()
    {
        return (gridSize.y * gridSize.x);
    }

    public Point gridSize()
    {
        return this.gridSize;
    }

    public Point cellSize()
    {
        return this.cellSize;
    }

    public Bitmap getCell(int position)
    {
        Point point = convertPositionToPoint(position);
        return getCell(point.y, point.x);
    }

    public Bitmap getCell(int row, int col)
    {
        return bitmapMatrix[row][col];
    }

    public void shuffle()
    {
        Random random = new Random();
        for (int i=0; i < (this.numberOfCells() * this.numberOfCells()); i++) {
            int posX = Math.abs(emptyCell.x + random.nextInt(3) - 1);
            int posY = Math.abs(emptyCell.y + random.nextInt(3) - 1);
            int position = (posY * gridSize.x) + posX;
            swapCells(position);
        }
    }

    public void swapCells(int position) {
        Point point = convertPositionToPoint(position);
        int diffX = point.x - emptyCell.x;
        int diffY = point.y - emptyCell.y;
        boolean swap = false;

        if (0 == diffY) {
            if (1 == diffX) {
                swap = true;  // Empty cell is on the left
            }
            else if (-1 == diffX) {
                swap = true;  // Empty cell is on the right
            }
            else {
                // Nothing to swap
            }
        }
        else if (0 == diffX) {
            if (1 == diffY) {
                swap = true;  // Empty cell is on the top
            }
            else if (-1 == diffY) {
                swap = true;  // Empty cell is at the bottom
            }
            else {
                // Nothing to swap
            }
        }
        else {
            // Nothing to swap
        }

        if (true == swap) {
            swapCells(point);
        }
    }

    private void swapCells(Point point)
    {
        // Do not swap if coordinates are outside the grid
        if (point.x < 0 || point.x >= gridSize.x || point.y < 0 || point.y >= gridSize.y) {
            Log.i("DEBUG", "Invalid cell [ " + point.x + ", " + point.y + " ]");
            return;
        }
        // Swap
        Bitmap emptyBitmap = bitmapMatrix[emptyCell.y][emptyCell.x];
        bitmapMatrix[emptyCell.y][emptyCell.x] = bitmapMatrix[point.y][point.x];
        bitmapMatrix[point.y][point.x] = emptyBitmap;
        emptyCell.x = point.x;
        emptyCell.y = point.y;
        Log.i("DEBUG", "Empty cell now at [ " + emptyCell.x + ", " + emptyCell.y + " ]");
    }

    private void initializeImage(Bitmap originalBitmap, Point displaySize)
    {
        // Compute ratio for image resizing
        double ratioX = originalBitmap.getWidth() / (double) displaySize.x;
        double ratioY = originalBitmap.getHeight() / (double) displaySize.y;
        double ratio;
        if (ratioX > ratioY) {
            ratio = ratioY;
        }
        else {
            ratio = ratioX;
        }
        int width = (int) (originalBitmap.getWidth() / ratio);
        int height = (int) (originalBitmap.getHeight() / ratio);

        // Scale image and compute width and height for cells
        Bitmap bitmap = Bitmap.createScaledBitmap(originalBitmap, width, height, false);
        cellSize.x = displaySize.x / gridSize.x;
        cellSize.y = displaySize.y / gridSize.y;

        // Divide it into a matrix of bitmaps
        for (int row = 0; row < gridSize.y; row++) {
            for (int col = 0; col < gridSize.x; col++) {
                int startY = row * cellSize.y;
                int startX = col * cellSize.x;
                bitmapMatrix[row][col] = Bitmap.createBitmap(bitmap, startX, startY, cellSize.x, cellSize.y);
            }
        }

        // Replace bottom left corner by an empty bitmap
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap emptyBitmap = Bitmap.createBitmap(cellSize.x, cellSize.y, conf);
        bitmapMatrix[emptyCell.y][emptyCell.x] = emptyBitmap;
    }

    private Point convertPositionToPoint(int position)
    {
        Point point = new Point();
        point.y = position / gridSize.x;
        point.x = position % gridSize.x;
        return point;
    }

}
