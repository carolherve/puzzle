package ca.ulex.puzzle;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class Main extends Activity
{
    private ImageMatrix imageMatrix;
    private boolean shuffled = false;
    private Point gridSize;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        // Get display width
        Display display = getWindowManager().getDefaultDisplay();
        Point outSmallestSize = new Point();
        Point outLargestSize = new Point();
        display.getCurrentSizeRange(outSmallestSize, outLargestSize);
        Point displaySize = new Point(outSmallestSize.x, outLargestSize.y);

        // Create image matrix object
        gridSize = new Point(3, 4);
        int imageID = R.drawable.sample_image_2;
        Bitmap bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), imageID);
        imageMatrix = new ImageMatrix(imageID, gridSize, bitmap, displaySize);

        // Create gridView
        GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setColumnWidth(imageMatrix.cellSize().x);
        gridView.setNumColumns(imageMatrix.gridSize().x);
        gridView.setAdapter(new ImageAdapter(this, imageMatrix));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if (false == shuffled) {
                    imageMatrix.shuffle();
                    shuffled = true;
                }
                else {
                    imageMatrix.swapCells(position);
                }
                ImageAdapter imageAdapter = (ImageAdapter) parent.getAdapter();
                imageAdapter.notifyDataSetChanged();
            }
        });
    }
}
